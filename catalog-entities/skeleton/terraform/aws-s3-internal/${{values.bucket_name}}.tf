module "s3-${{ values.bucket_name }}" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "${{ values.bucket_name }}"
  acl    = "private"

  control_object_ownership = true
  object_ownership         = "ObjectWriter"

  versioning = {
    enabled = false
  }

  tags = {
    ef_departamento = "${{ values.ef_departamento }}"
    ef_descricao    = "${{ values.ef_descricao }}"
    ef_owner        = "${{ values.ef_owner }}"
    ef_projeto      = "${{ values.ef_projeto }}"
    ef_regiao       = "${{ values.region }}"
    ef_iac          = "terraform"
    ef_provedor     = "aws"
  }

}